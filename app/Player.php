<?php

class Player {
    
    public $x;
    public $y;
    public $color;
    public $ind;
    public $size;
    protected $_uid;
    protected $_model;
    public $name;
    
    public function setUid($uid){
        $this->_uid = $uid;
        $model = $this->getModel();
        $this->name = $model->name;
    }
    
    public function getUid(){
        return $this->_uid;
    }
    
    public function getModel(){
        if ($this->_uid && $this->_model === null){
            $this->_model = User::model()->findByPk($this->_uid);
        }
        return $this->_model;
    }
}