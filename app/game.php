<?php

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

require __DIR__ . '/Ratchet/vendor/autoload.php';
require 'MazeGenerator.php';
require "class.maze.php";
require "Player.php";

class Game implements MessageComponentInterface {

    protected $clients;
    protected $_mazeGenerator;
    protected $_mazeSize = 70;
    
    protected $_playerStartPositions = array(
        array(0, 0),
    );
    
    protected $_playerColors = array(
        '#0f0',
        '#00f',
        '#f0f',
        '#f60',
        '#0ff',
        '#626',
        '#266',
        '#660',
    );
    
    protected $_playerSize = 10;
    
    protected $_players = array();
    
    protected $_freePlayerIndexies = array();
    protected $_maxPlayers = 6;
    
    protected $_applePos = array();
    protected $_gameIsOver = false;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        
        $this->_playerStartPositions[] = array($this->_mazeSize * $this->_playerSize - $this->_playerSize, $this->_mazeSize * $this->_playerSize - $this->_playerSize);
        $this->_playerStartPositions[] = array($this->_mazeSize * $this->_playerSize - $this->_playerSize, 0);
        $this->_playerStartPositions[] = array(0, $this->_mazeSize * $this->_playerSize - $this->_playerSize);
        
        for ($i=0; $i<$this->_maxPlayers; $i++){
            $this->_freePlayerIndexies[] = $i;
        }
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);     
        $this->_initConnection($conn);
    }
    
    public function onMessage(ConnectionInterface $from, $msg) {
        $data = json_decode($msg);
        $hash = $data->hash;
        switch ($data->action) {
            case 'move':
                $player = $this->_getPlayerByClient($from);
                if ($this->_getClientHash($from) === $hash && $player) {
                    if ($this->_isAbleToMove($player, $data->direction)) {
                        $this->_movePlayer($player, $data->direction);
                        $this->_checkState($from);
                    }
                }
                break;
            case 'reset':
                $data = new stdClass();
                $data->action = 'reset';
                foreach ($this->clients as $client) {
                    $client->send(json_encode($data));
                }
                $this->_mazeGenerator->generate();
                $this->_gameIsOver = false;
                break;
            case 'join':
                if ($data->uid) {
                    $user = User::model()->findByPk($data->uid);
                    if ($user) {
                        $this->_initMaze($from);
                        $this->_initApple($from);
                        $this->_sendExistedPlayers($from);
                        $this->_addPlayer($from, $data->uid);
                    }
                }
                
                
                break;
        }
    }
    
    /**
     * 
     * @param type $client
     * @return Player
     */
    protected function _getPlayerByClient($client) {
        $hash = $this->_getClientHash($client);
        if (!empty($this->_players[$hash])) {
            return $this->_players[$hash];
        }
    }
    
    protected function _movePlayer(Player $player, $direction){
        $ox = 0;
        $oy = 0;
        $stepSize = $this->_playerSize;
        switch ($direction){
            case 'up':
                $oy -= $stepSize;
                break;
            case 'right':
                $ox += $stepSize;
                break;
            case 'down':
                $oy += $stepSize;
                break;
            case 'left':
                $ox -= $stepSize;
                break;
        }
        $player->x += $ox;
        $player->y += $oy;
        
        $data = new stdClass();
        $data->action = 'movePlayer';
        $data->player = $player;
        foreach ($this->clients as $client) {
            $client->send(json_encode($data));
        }
    }
    
    protected function _checkState($client) {
        $player = $this->_getPlayerByClient($client);
        if (!$this->_gameIsOver && $player) {
            $winnerPlayer = false;
            if ($player->x == $this->_applePos[0] && $player->y == $this->_applePos[1]) {
                $this->_gameIsOver = true;

                $data = new stdClass();
                $data->action = 'gameIsOver';
                foreach ($this->clients as $c) {
                    if ($client == $c) {
                        $data->result = 'win';
                        $winnerPlayer = $player;
                    } else {
                        $data->result = 'lose';
                    }
                    $c->send(json_encode($data));
                }
                
                if ($winnerPlayer) {
                    $winnerPlayer->getModel()->addWin(count($this->_players) > 1);
                }
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $hash = $this->_getClientHash($conn);
        if (!empty($this->_players[$hash])){
            $this->_removePlayer($this->_players[$hash]);
            unset($this->_players[$hash]);
            
        }
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }
    
    protected function _initConnection($client){
        $data = new stdClass();
        $data->action = 'initConnection';
        $data->hash = $this->_getClientHash($client);
        $data->playerSize = $this->_playerSize;
        $data->mazeSize = $this->_mazeSize;
        $client->send(json_encode($data));
    }

    protected function _initMaze($client) {
        if (!$this->_mazeGenerator) {
            $this->_mazeGenerator = new MazeGenerator();
            $this->_mazeGenerator->setSize($this->_mazeSize);
            $this->_mazeGenerator->setCellSize($this->_playerSize);
            $points = $this->_mazeGenerator->generate();
        } else {
            $points = $this->_mazeGenerator->getLines();
        }
        $data = new stdClass();
        $data->action = 'drawMaze';
        $data->data = $points;
        $client->send(json_encode($data));
    }
    
    protected function _initApple($client){
        $cellSize = $this->_mazeGenerator->getCellSize();
        $size = $this->_mazeGenerator->getSize();
        $center = $size / 2 * $cellSize;
        $this->_applePos = array($center, $center);
        
        $data = new stdClass();
        $data->action = 'setApple';
        $data->applePos = $this->_applePos;
        $client->send(json_encode($data));
    }
    
    protected function _sendExistedPlayers($client){
        $data = new stdClass();
        $data->action = 'addPlayers';
        $data->players = array();
        foreach ($this->_players as $player){
            $data->players[] = $player;
        }
        $client->send(json_encode($data));
    }
    
    protected function _addPlayer($ownerClient, $uid){
        $ind = $this->_getNextFreeInd();
        if ($ind === null){
            return;
        }
        $ownerHash = $this->_getClientHash($ownerClient);
        $player = new Player();
        $player->setUid($uid);
        $player->x = $this->_playerStartPositions[$ind%4][0];
        $player->y = $this->_playerStartPositions[$ind%4][1];
        $player->color = $this->_playerColors[$ind];
        $player->ind = $ind;
        
        $ownData = new stdClass();
        $ownData->action = 'myPlayerInd';
        $ownData->ind = $ind;
        $ownerClient->send(json_encode($ownData));
        
        $data = new stdClass();
        $data->action = 'addPlayer';
        $data->player = $player;
        
        foreach ($this->clients as $client) {
            $client->send(json_encode($data));
        }
        $this->_players[$ownerHash] = $player;
    }
    
    protected function _removePlayer($player){
        $data = new stdClass();
        $data->action = 'removePlayer';
        $data->playerInd = $player->ind;
        foreach ($this->clients as $client) {
            $client->send(json_encode($data));
        }
        array_unshift($this->_freePlayerIndexies, $player->ind);
    }
    
    protected function _getClientHash($client){
        $hash = md5($client->resourceId . ".__SALT__.");
        return $hash;
    }
    
    protected function _isAbleToMove(Player $player, $direction) {
        $maze = $this->_mazeGenerator->getMaze();
        $cellSize = $this->_mazeGenerator->getCellSize();
        $size = $this->_mazeGenerator->getSize();
        $posX = $player->x;
        $posY = $player->y;

        $i = $posX / $cellSize + 1;
        $j = $posY / $cellSize + 1;

        $flag = true;
        
        
        if ($j==1 && $direction == 'up'){
            $flag = false;
        }
        if ($j==$size && $direction == 'down'){
            $flag = false;
        }
        if (($maze->maze[$i][$j] & 1) != 0 && $direction == 'up') /* This cell has a top wall */
            $flag = false;
        if (($maze->maze[$i][$j] & 2) != 0 && $direction == 'down') /* This cell has a bottom wall */
            $flag = false;
        if (($maze->maze[$i][$j] & 4) != 0 && $direction == 'left') /* This cell has a left wall */
            $flag = false;
        if (($maze->maze[$i][$j] & 8) != 0 && $direction == 'right') /* This cell has a right wall */
            $flag = false;
        return $flag;
    }
    
    protected function _getNextFreeInd(){
        return array_shift($this->_freePlayerIndexies);
    }

}

$server = IoServer::factory(new WsServer(new Game), Yii::app()->params['mazeWebSocketServerPort']);
$server->run();