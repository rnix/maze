<?php

class MazeGenerator {
    
    protected $_size = 10;
    protected $_maze;
    protected $_cellSize = 10;
    protected $_lines;
    
    public function setSize($size){
        $this->_size = $size;
    }
    
    public function setCellSize($cellSize){
        $this->_cellSize = $cellSize;
    }
    
    public function generate() {
        $lines = array();
        $maze = new Maze($this->_size, $this->_size);
        $xs = $this->_cellSize;
        $ys = $this->_cellSize;
        for ($j = 1; $j < $maze->my - 1; ++$j) {
            for ($i = 1; $i < $maze->mx - 1; ++$i) {
                $x = $i - 1;
                $y = $j - 1;
                if (($maze->maze[$i][$j] & 1) != 0) /* This cell has a top wall */
                    $lines[] = array($x * $xs, $y * $ys, $x * $xs + $xs, $y * $ys);
                if (($maze->maze[$i][$j] & 2) != 0) /* This cell has a bottom wall */
                    $lines[] = array($x * $xs, $y * $ys + $ys, $x * $xs + $xs, $y * $ys + $ys);
                if (($maze->maze[$i][$j] & 4) != 0) /* This cell has a left wall */
                    $lines[] = array($x * $xs, $y * $ys, $x * $xs, $y * $ys + $ys);
                if (($maze->maze[$i][$j] & 8) != 0) /* This cell has a right wall */
                    $lines[] = array($x * $xs + $xs, $y * $ys, $x * $xs + $xs, $y * $ys + $ys);
            }
        }
        $this->_maze = $maze;
        $this->_lines = $lines;
        
        return $lines;
    }

    public function getMaze(){
        return $this->_maze;
    }
    
    public function getCellSize(){
        return $this->_cellSize;
    }
    
    public function getSize(){
        return $this->_size;
    }
    
    public function getLines(){
        return $this->_lines;
    }
}