<?php
/*~ class.maze.php
.-----------------------------------------------------------------------------------.
|  Software: Maze generator                                                         |
|   Version: 1.1.0                                                                  |
|   Contact: http://dev.horemag.net                                                 |
| --------------------------------------------------------------------------------- |
|          Author: http://en.wikipedia.org/wiki/User:Cyp                            |
| PHP Addaptation: Evgeni Vasilev (original founder)                                |
| --------------------------------------------------------------------------------- |
|   License: GNU Free Documentation License                                         |
| http://en.wikipedia.org/wiki/Wikipedia:Text_of_the_GNU_Free_Documentation_License |
| This program is distributed in the hope that it will be useful - WITHOUT          |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or             |
| FITNESS FOR A PARTICULAR PURPOSE.                                                 |
| --------------------------------------------------------------------------------- |
| 01.03.2008 - Added SVG Export                                                     |
'----------------------------------------------------------------------------------'

/**
 * Maze - PHP maze generator class
 * @package Maze
 * @author Evgeni Vasilev
 */

class Maze{
  var $maze = array();
  var $mx = 0;
  var $my = 0;
  var $debug = false;

  function __construct($mx, $my){
    $mx +=2;
    $my +=2;
    $this->mx = $mx;
    $this->my = $my;
    $dx = array( 0, 0, -1, 1 );
    $dy = array( -1, 1, 0, 0 );
    $todo = array(); 
    $todonum = 0;

    for ($x = 0; $x < $mx; ++$x){
      for ($y = 0; $y < $my; ++$y){
        if ($x == 0 || $x == $mx-1 || $y == 0 || $y == $my-1) {
          $this->maze[$x][$y] = 32;
        } else {
          $this->maze[$x][$y] = 63;
        }
      }
    }

    $x = rand(1, $mx-2);
    $y = rand(1, $my-2);
    $x = 1;
    $y = 1;

    $this->maze[$x][$y] &= ~48;

    for ($d = 0; $d < 4; ++$d){
      if (($this->maze[$x + $dx[$d]][$y + $dy[$d]] & 16) != 0) {
        $todo[$todonum++] = (($x + $dx[$d]) << 16) | ($y + $dy[$d]);
        $this->maze[$x + $dx[$d]][$y + $dy[$d]] &= ~16;
      }
    }

    while ($todonum > 0) {
      // We select one of the squares next to the maze.

      if ($this->debug)
        echo $this->render($x, $y);

      $n = rand(0, $todonum-1);

      $x = $todo[$n] >> 16; // the top 2 bytes of the data 
      $y = $todo[$n] & 65535; // the bottom 2 bytes of the data

      // We will connect it, so remove it from the queue.
      $todo[$n] = $todo[--$todonum];

      // Select a direction, which leads to the maze. 
      do {
        $d = rand(0, 3);
      } while (($this->maze[$x + $dx[$d]][$y + $dy[$d]] & 32) != 0);

      // Connect this square to the maze.
      $this->maze[$x][$y] &= ~((1 << $d) | 32);
      $this->maze[$x + $dx[$d]][$y + $dy[$d]] &= ~(1 << ($d ^ 1));

      // Remember the surrounding squares, which aren't queued
      for ($d = 0; $d < 4; ++$d){
        if (($this->maze[$x + $dx[$d]][$y + $dy[$d]] & 16) != 0) {
          // connected to the maze, and aren't yet queued to be
          $todo[$todonum++] = (($x + $dx[$d]) << 16) | ($y + $dy[$d]);
          $this->maze[$x + $dx[$d]][$y + $dy[$d]] &= ~16;
        }
        // Repeat until finished.
      }
    }
    $this->maze[1][1] &= ~1;
    $this->maze[$mx-2][$my-2] &= ~2;
  }

  function render($rx = -1, $ry = -1){
    $t = "<table>\n";
    for ($y = 1; $y < $this->my-1; ++$y) {
      $t .= "  <tr>\n";
      for ($x = 1; $x < $this->mx-1; ++$x){
        $c = '';
        if ($x == $rx && $y == $ry){
          $c = 'r ';
        }
        if (($this->maze[$x][$y] & 1) != 0) /* This cell has a top wall */
          $c .= 'tw ';
        if (($this->maze[$x][$y] & 2) != 0) /* This cell has a bottom wall */
          $c .= 'bw ';
        if (($this->maze[$x][$y] & 4) != 0) /* This cell has a left wall */
          $c .= 'lw ';
        if (($this->maze[$x][$y] & 8) != 0) /* This cell has a right wall */
          $c .= 'rw ';
        $t .= "  <td class=\"$c\">&nbsp;</td>\n";
      }
      $t .= "  </tr>\n";
    }
    $t .= "</table>\n";
    return $t;
  }
  
  
  function _drawLine($x1, $y1, $x2, $y2){
    return "    <line x1=\"$x1\" y1=\"$y1\" x2=\"$x2\" y2=\"$y2\"/>\n";
  }
  function renderSVG($xs, $ys){
    $off = 10;
    $w = ($this->mx*$xs)+($off*2);
    $h = ($this->my*$ys)+($off*2);
    $t =  '<?xml version="1.0" standalone="no"?>'."\n";
    $t .= '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">'."\n";
    $t .= "<svg width=\"$w\" height=\"$h\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n";
    $t .= '  <g stroke="black">'."\n";
    for ($y = 1; $y < $this->my-1; ++$y) {
      for ($x = 1; $x < $this->mx-1; ++$x){
        if (($this->maze[$x][$y] & 1) != 0) /* This cell has a top wall */
          $t .= $this->_drawLine ($x * $xs + $off, $y * $ys + $off, $x * $xs + $xs + $off, $y * $ys + $off);
        if (($this->maze[$x][$y] & 2) != 0) /* This cell has a bottom wall */
          $t .= $this->_drawLine ($x * $xs + $off, $y * $ys + $ys + $off, $x * $xs + $xs + $off, $y * $ys + $ys + $off);
        if (($this->maze[$x][$y] & 4) != 0) /* This cell has a left wall */
          $t .= $this->_drawLine ($x * $xs + $off, $y * $ys + $off, $x * $xs + $off, $y * $ys + $ys + $off);
        if (($this->maze[$x][$y] & 8) != 0) /* This cell has a right wall */
          $t .= $this->_drawLine ($x * $xs + $xs + $off, $y * $ys + $off, $x * $xs + $xs + $off, $y * $ys + $ys + $off);
      }
    }
    $t .= '  </g>'."\n";
    $t .= '</svg>'."\n";
    return $t;
  }
  
  function getStyle(){
    $style = 
'<style type="text/css">
table{font-size: 3px; border-collapse:collapse;float:left; margin: 5px}
td.tw{border-top:2px solid black}
td.bw{border-bottom:2px solid black}
td.lw{border-left:2px solid black}
td.rw{border-right:2px solid black}
td.r{background-color: red;}
td{width:5px; height:5px;}
</style>';
    return $style;
  }
}
?>