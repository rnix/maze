<?php

defined('YII_DEBUG') || define('YII_DEBUG', false);
defined('YII_TRACE_LEVEL') || define('YII_TRACE_LEVEL', 3);

$configDir = dirname(__FILE__) . '/../protected/config';

require_once('/yii/yii-1.1.13/framework/yii.php');

$config = require_once( $configDir . '/main.php' );
$localConfigFile = $configDir . '/server.local.php';
if (file_exists($localConfigFile)) {
    $configServer = require_once( $localConfigFile );
    $config = CMap::mergeArray($config, $configServer);
}

Yii::createWebApplication($config)->run();
