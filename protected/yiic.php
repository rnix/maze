<?php
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once('/yii/yii-1.1.13/framework/yii.php');
$yiic = '/yii/yii-1.1.13/framework/yiic.php';

$configDir = dirname(__FILE__) . '/../protected/config';
$config = require_once( $configDir . '/main.php' );
$localConfigFile = $configDir . '/server.local.php';
if (file_exists($localConfigFile)) {
    $configServer = require_once( $localConfigFile );
    $config = CMap::mergeArray($config, $configServer);
}

require_once($yiic);
