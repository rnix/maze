<?php

class MazeController extends Controller {

    
    public function actionGame() {
        if (Yii::app()->user->isGuest){
            Yii::app()->user->setReturnUrl(array('maze/game'));
            $this->redirect(array('site/selectLogin'));
        }
        $this->render('game');
    }

}