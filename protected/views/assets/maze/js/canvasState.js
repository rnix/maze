function CanvasState(canvas) {
        
    this.canvas = canvas;
    this.width = canvas.width;
    this.height = canvas.height;
    this.ctx = canvas.getContext('2d');
    this.valid = false;
    this.players = {}; 
    this.maze = null;
    this.apple = null;
    this.playerSize = 5;

    var myState = this;
    
    this.interval = 30;
    setInterval(function() {
        myState.draw();
    }, myState.interval);
}

CanvasState.prototype.clear = function() {
  this.ctx.clearRect(0, 0, this.width, this.height);
}


CanvasState.prototype.addPlayer = function(ind, player) {
    this.players[ind] = player;
    this.valid = false;
}

CanvasState.prototype.movePlayerByInd = function(ind, newX, newY) {
    if (this.players[ind]){
        this.players[ind].x = newX;
        this.players[ind].y = newY;
        this.valid = false;
    } else {
        throw "Player not found ("+ind+")";
    }
    
}

CanvasState.prototype.removePlayer = function(ind) {
    delete this.players[ind];
    this.valid = false;
}

CanvasState.prototype.setMaze = function(maze) {
    this.maze = maze;
    this.valid = false;
}

CanvasState.prototype.setApple = function(apple) {
    this.apple = apple;
    this.valid = false;
}
    
CanvasState.prototype.draw = function() {
    if (!this.valid) {
        var ctx = this.ctx;
        var players = this.players;
        this.clear();
 
        if (this.maze){
            this.maze.draw(ctx);
        }
        
        if (this.apple){
            this.apple.draw(ctx, this.playerSize);
        }
        
        for(var ind in players){
            if (typeof players[ind] != 'undefined'){
                players[ind].draw(ctx, this.playerSize);
            }
            
        }
        
        this.valid = true;
    }
}