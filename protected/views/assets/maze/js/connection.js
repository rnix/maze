var DEBUG = false;
$(function(){
    var ws;
    if ("WebSocket" in window){
        ws = new WebSocket(mazeWebSocketAddress);
        ws.onopen = function(){
            start();
        };
        ws.onmessage = function(evt){
            onMessage(evt);
        };
        ws.onclose = function() {
            setTimeout(function(){
                location.href = location.href;
            }, 2000);
        };
    } else {
        alert('Ваш браузер не поддерживает вебсокеты');
    }
    
    var canvasState;
    
    var keyUp = 38;
    var keyW = 87;
    var keyRight = 39;
    var keyD = 68;
    var keyDown = 40;
    var keyS = 83;
    var keyLeft = 37;
    var keyA = 65;
    
    var myConnectionHash = "";
    var myPlayerInd = -1;
    var gameIsOver = false;
    var canReset = false;
        
    var start = function(){
        var data = {"action": "join", "uid": siteUid};
        sendData(data);
    };
    
    
    var onMessage = function (evt){
        var data = $.parseJSON(evt.data);
        if (DEBUG) console.log('input', data);
        
        switch(data.action){
            case 'initConnection': 
                myConnectionHash = data.hash;
                var canvas = document.getElementById('box');
                canvas.width = data.mazeSize * data.playerSize;
                canvas.height = data.mazeSize * data.playerSize;
                canvasState = new CanvasState(canvas);
                canvasState.playerSize = data.playerSize;
                $(".main").width(canvas.width);
                
                setTimeout(function(){
                    canReset = true;
                    $(".reset").show();
                }, 4000);
                
                break;
            case 'drawMaze': 
                canvasState.setMaze(new Maze(data.data));
                document.onkeydown = onKeyDown;
                break;
            case 'addPlayer':
                var p = data.player;
                canvasState.addPlayer(p.ind, new Player(p));
                addPlayerInList(p);
                break;
            case 'movePlayer':
                var p = data.player;
                canvasState.movePlayerByInd(p.ind, p.x, p.y);
                break;
            case 'addPlayers':
                var players = data.players;
                var p;
                for (var i=0; i<players.length; i++){
                    p = players[i];
                    canvasState.addPlayer(p.ind, new Player(p));
                    addPlayerInList(p);
                }
                break;
            case 'removePlayer':
                canvasState.removePlayer(data.playerInd);
                break;
            case 'setApple': 
                canvasState.setApple(new Apple(data.applePos));
                break;
            case 'gameIsOver': 
                gameIsOver = true;
                if (data.result == 'win'){
                    $(".game-is-over.win").show();
                } else {
                    $(".game-is-over.lose").show();
                }
                canReset = true;
                $(".reset").show();
                break;
            case 'reset': 
                location.href = location.href;
                break;
            case 'myPlayerInd': 
                myPlayerInd = data.ind;
                break;
        }
    }
    
    $(".reset").click(function(){
        if (canReset){
            sendData({'action': 'reset'});
        }
    });
    
    var onKeyDown = function(evt){
        if (gameIsOver){
            return;
        }
        switch(evt.keyCode){
            case keyUp:
            case keyW:
                moveMyPlayer('up');
                break;
            case keyRight:
            case keyD:
                moveMyPlayer('right');
                break;
            case keyDown:
            case keyS:
                moveMyPlayer('down');
                break;
            case keyLeft:
            case keyA:
                moveMyPlayer('left');
                break;
            
        }
    }
    
    var moveMyPlayer = function(direction){
        var data = {'action': 'move', 'direction': direction};
        sendData(data);
    }
    
    var sendData = function (data){
        data['hash'] = myConnectionHash;
        if (DEBUG) console.log('output', data);
        ws.send(JSON.stringify(data));
    }
    
    var fromLocalCoordSystem = function(pos){
        return {'x': pos.x + $box.offset().left, 'y': pos.y + $box.offset().top};
    }
    
    var toLocalCoordSystem = function(pos){
        return {'x': pos.x - $box.offset().left, 'y': pos.y - $box.offset().top};
    }
    
    var playersList = $(".players-list")[0];
    function addPlayerInList(player){
        if (!$("li[data-ind='" + player.ind + "']", playersList).length) {
            var $tpl = $(".players-list .tpl").clone();
            $tpl.removeClass('tpl');
            $tpl.find('.player-color').css('backgroundColor', player.color);
            $tpl.find('.player-name').html(player.name);
            $tpl.attr('data-ind', player.ind);
            if (player.ind === myPlayerInd){
                $tpl.addClass('my-player');
            }
            $tpl.appendTo($(".players-list"));
            $tpl.show();
        }
    }
});