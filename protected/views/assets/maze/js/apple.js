function Apple(pos) { 
    this.x = pos[0] || 0;
    this.y = pos[1] || 0;
    this.fill = '#f00';
}

Apple.prototype.draw = function(ctx, playerSize) {
  var radius = playerSize / 2;
  ctx.beginPath();
  ctx.fillStyle = this.fill;
  ctx.arc(this.x+radius, this.y+radius, radius, 0 , 2 * Math.PI, false);
  ctx.fill();
  ctx.stroke();
}
    