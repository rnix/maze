function Player(playerObj) { 
    this.x = playerObj.x || 0;
    this.y = playerObj.y || 0;
    this.fill = playerObj.color || '#AAAAAA';
}

Player.prototype.draw = function(ctx, playerSize) {
  ctx.fillStyle = this.fill;
  var offset = 2;
  ctx.fillRect(this.x + offset, this.y + offset,  playerSize - 2*offset,  playerSize - 2*offset);
  
  ctx.fillStyle = '#000000';
  offset = offset*1.8;
  ctx.fillRect(this.x + offset, this.y + offset,  playerSize - 2*offset,  playerSize - 2*offset);
}
    