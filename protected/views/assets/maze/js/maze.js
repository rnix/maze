function Maze(lines, fill) { 
    this.lines = lines;
    this.fill = fill || '#AAAAAA';
}

Maze.prototype.draw = function(ctx) {
    ctx.beginPath();
    ctx.fillStyle = this.fill;
    var m = this.multiplier;
    var k;
    for (var i=0; i<this.lines.length; i++){
        k = this.lines[i];
        ctx.moveTo(k[0], k[1]);
        ctx.lineTo(k[2], k[3]);
    }
    ctx.stroke();
}