<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="hero-unit">
    <h1>Добро пожаловать на <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
    <p>
        Это очень простая многопользовательская онлайн игра, где главная задача - добраться первым в центр случайного лабиринта.
        Если вам не интересно соперничество, то вы можете просто поиграть в прохождение лабиринта в одиночку.
    </p>
    <p>
        <a class="btn btn-primary btn-large" href="<?= $this->createUrl('maze/game') ?>">
            Играть
        </a>
    </p>
</div>