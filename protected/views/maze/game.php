<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - Game';
?>
<script>
    var mazeWebSocketAddress = "<?= Yii::app()->params['mazeWebSocketAddress'] ?>";
    var siteUid = "<?= Yii::app()->user->getModel()->uid ?>";
</script>
<?php
$mazePackage = array(
    'basePath' => 'application.views.assets.maze',
    'css' => array('css/style.css',),
    'js' => array('js/connection.js', 'js/canvasState.js', 'js/maze.js', 'js/player.js', 'js/apple.js'),
    'depends' => array('jquery')
);

Yii::app()->assetManager->publish(Yii::getPathOfAlias($mazePackage['basePath']), false, -1, defined('YII_DEBUG') && YII_DEBUG);
Yii::app()->clientScript->addPackage('mazePack', $mazePackage)->registerPackage('mazePack');
?>
<div class="row">


    <div class="main span8">
        <div><em>Доберись в центр первым! Используй клавиши W, A, S, D.</em></div>

        <canvas class="box" width="100" height="100" id="box">
        </canvas>

        <div class="game-is-over win" style="display: none;">
            YOU WIN!
        </div>
        <div class="game-is-over lose" style="display: none;">
            YOU LOSE!
        </div>
        <button class="reset" style="display: none;">reset</button>
    </div>
    <div class="span4">
        <h4>Список игроков</h4>
        <ol class="players-list">
            <li class="tpl player-in-list" style="display: none;">
                <div class="player-color">&nbsp;</div><div class="player-name">Player</div>
            </li>
        </ol>
    </div>

</div>