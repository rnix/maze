<?php
/* Edit and rename this file to server.local.php */
return array(
    'params' => array(
        'mazeWebSocketAddress' => 'ws://localhost:8079',
        'mazeWebSocketServerPort' => '8079',
        'livelevelAchs' => array(
            'one_victory' => 0,
        ),
    ),
    'components' => array(
        'eauth' => array(
            'services' => array(
                'livelevel' => array(
                    'client_id' => '',
                    'client_secret' => '',
                ),
            ),
        ),
    )
);
